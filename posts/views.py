from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView
from .models import Posts


class HomePageView(ListView):
    model = Posts
    template_name = 'home.html'
    context_object_name = 'all_posts_list' # for when used in templates/home and getting the name it will be easier
    # and much more clear than default object_list
